﻿using System;
using System.Collections.Generic;
using System.Linq;
using AngleSharp.Dom;
using AngleSharp.Dom.Html;
using AngleSharp.Parser.Html;
using System.IO;
using System.Net;
using System.Globalization;
using System.Threading;

namespace tokenmarket
{
    public class tm
    {
        public class IcoInfo
        {
            public string imageurl { get; set; }
            public string status { get; set; }
            public string name { get; set; }
            public string fulldescrsource { get; set; }
            public string symbol { get; set; }
            public string descr { get; set; }
            public FullDescription fulldescr { get; set; } = new FullDescription();
            public class FullDescription
            {
                public YouTube youtube { get; set; } = new YouTube();
                public Overview overview { get; set; } = new Overview();
                public Links links { get; set; } = new Links();
                public LatestTweets latesttweets { get; set; } = new LatestTweets();
                public Team team { get; set; } = new Team();
                public ArticlesAndResearch articlesandresearch { get; set; } = new ArticlesAndResearch();
                public LatestNews latestnews { get; set; } = new LatestNews();
                public Technology technology { get; set; } = new Technology();
                public GitHub github { get; set; } = new GitHub();
                public LegalStructure legalstructure { get; set; } = new LegalStructure();
                public LatestPosts latestposts { get; set; } = new LatestPosts();
                public WebPresence webpresence { get; set; } = new WebPresence();

                public class YouTube
                {
                    public string source { get; set; }
                    public string channel { get; set; }
                }
                public class Overview
                {
                    public Dictionary<string, string> info { get; set; } = new Dictionary<string, string>();
                }
                public class Links
                {
                    public Dictionary<string, string> links { get; set; } = new Dictionary<string, string>();
                }
                public class LatestTweets
                {
                    public List<string> tweets { get; set; } = new List<string>();
                    public int followers { get; set; }
                    public int posts { get; set; }
                }
                public class Team
                {
                    public List<Member> members { get; set; } = new List<Member>();
                    public class Member
                    {
                        public string name { get; set; }
                        public string position { get; set; }
                    }

                    public string country { get; set; }
                }
                public class ArticlesAndResearch
                {
                    public List<Researche> researches { get; set; } = new List<Researche>();
                    public class Researche
                    {
                        public string content { get; set; }
                        public string source { get; set; }
                    }

                }
                public class LatestNews
                {
                    public List<News> news { get; set; } = new List<News>();
                    public class News
                    {
                        public string content { get; set; }
                        public string href { get; set; }
                    }
                }
                public class Technology
                {
                    public Dictionary<string, string> tecnhology { get; set; } = new Dictionary<string, string>();
                }
                public class GitHub
                {
                    public Dictionary<string, int> gitstats { get; set; } = new Dictionary<string, int>();
                }
                public class LegalStructure
                {
                    public string structure { get; set; }
                }
                public class LatestPosts
                {
                    public List<Post> posts { get; set; } = new List<Post>();
                    public class Post
                    {
                        public string content { get; set; }
                        public string href { get; set; }
                    }

                }
                public class WebPresence
                {
                    public double domainscore { get; set; }
                    public int backlinks { get; set; }
                }
            }
        }

        public List<IcoInfo> GetAllTokens()
        {
            string baseurl = "https://tokenmarket.net";
            List<IcoInfo> tokens = new List<IcoInfo>();
            var doc = GetDocByUrl(baseurl + "/blockchain/all-assets?sorting=best&archived=true&all=true");
            var icotable = doc.QuerySelectorAll("table").Where(q => q.ClassName.Contains("table table-listing table-assets")).FirstOrDefault().QuerySelector("tbody");
            var icos = icotable.QuerySelectorAll("tr");
            foreach (var ico in icos)
            {
                tokens.Add(GetDescription(ico));
            }        
            return tokens;
        }
        public IcoInfo GetToken(string tokenname)
        {
            string baseurl = "https://tokenmarket.net";
            var doc = GetDocByUrl(baseurl + "/blockchain/all-assets?sorting=best&archived=true&all=true");
            var icotable = doc.QuerySelectorAll("table").Where(q => q.ClassName.Contains("table table-listing table-assets")).FirstOrDefault().QuerySelector("tbody");
            var ico = icotable.QuerySelectorAll("tr").Where(q => q.TextContent.Contains(tokenname)).FirstOrDefault();
            return GetDescription(ico);
        }
        private IcoInfo GetDescription(IElement ico)
        {
            IcoInfo ii = new IcoInfo();

            try { ii.imageurl = Validate(ico.QuerySelectorAll("img").LastOrDefault().Attributes["src"].Value); } catch { }
            try { ii.status = Validate(ico.QuerySelectorAll("td").Where(q => q.ClassName == "col-asset-status").FirstOrDefault().TextContent); } catch { }
            try { ii.name = Validate(ico.QuerySelectorAll("td").Where(q => q.ClassName == "col-asset-name").FirstOrDefault().TextContent); } catch { }
            try { ii.fulldescrsource = Validate(ico.QuerySelectorAll("a").Where(q => q.Attributes["href"].Value.Contains("https://tokenmarket.net/")).FirstOrDefault().Attributes["href"].Value);} catch { }
            try { ii.symbol = Validate(ico.QuerySelectorAll("td").Where(q => q.ClassName == "col-asset-symbol").FirstOrDefault().TextContent); } catch { }
            try { ii.descr = Validate(ico.QuerySelectorAll("td").Where(q => q.ClassName == "col-asset-description").FirstOrDefault().TextContent); } catch { }
            ii.fulldescr = GetFullDescription(ii.fulldescrsource);
            return ii;
        }
        private IcoInfo.FullDescription GetFullDescription(string url)
        {
            IcoInfo.FullDescription descr = new IcoInfo.FullDescription();
            var doc = GetDocByUrl(url);
            if (!doc.Title.Contains("TokenMarket - Token sales"))
            {
                descr.youtube = GetYoutube(descr.youtube, doc);

                var overview = GetTable(doc, "Overview");
                if (overview == null) descr.overview = null;
                else foreach (var item in overview.QuerySelectorAll("tr"))
                    {
                        descr.overview.info.Add(Validate(item.QuerySelector("th").TextContent), Validate(item.QuerySelector("td").TextContent));
                    }

                var linkstable = GetTable(doc, "Links");
                if (linkstable == null) descr.links = null;
                else foreach (var link in linkstable.QuerySelectorAll("tr"))
                    {
                        string linkname;
                        linkname = Validate(link.TextContent);
                        if (link.TextContent.Contains("(")) linkname = linkname.Substring(0, linkname.IndexOf('('));
                        descr.links.links.Add(linkname.Replace(" ", ""), Validate(GetLink(doc, linkname)));
                    }

                var latesttweets = GetTable(doc, "Latest tweets");
                if (latesttweets == null) descr.latesttweets = null;
                else foreach (var item in latesttweets.QuerySelectorAll("a"))
                    {
                        descr.latesttweets.tweets.Add(Validate(item.TextContent));
                    }
                descr.latesttweets = GetTweetsAndFollowersCount(descr.latesttweets, descr.links.links["Twitter"]);

                var team = GetTable(doc, "Team");
                if (team == null || doc.Children[0].TextContent.Contains("Team member information missing.")) descr.team = null;
                else
                {
                    foreach (var item in team.QuerySelectorAll("p"))
                    {
                        string[] meminfo = item.TextContent.Replace(" - ", "~").Split('~');
                        if (meminfo.Length == 2)
                            descr.team.members.Add(new IcoInfo.FullDescription.Team.Member { name = Validate(meminfo[0]), position = Validate(meminfo[1]) });
                    }
                    descr.team.country = Validate(team.QuerySelectorAll("tr").Where(q => q.TextContent.Contains("Country of origin")).FirstOrDefault().QuerySelector("td").TextContent);
                }
                var articlesandresearch = GetTable(doc, "Articles and research");
                if (articlesandresearch == null) descr.articlesandresearch = null;
                else foreach (var item in articlesandresearch.QuerySelectorAll("td"))
                    {
                        if (item.Children[0].TextContent != "")
                            descr.articlesandresearch.researches.Add(new IcoInfo.FullDescription.ArticlesAndResearch.Researche { content= Validate(item.Children[0].TextContent), source = Validate(item.Children[1].TextContent) });
                    }

                var latestnews = GetTable(doc, "Latest news");
                if (latestnews == null) descr.latestnews = null;
                else foreach (var item in latestnews.QuerySelectorAll("a"))
                    {
                        descr.latestnews.news.Add(new IcoInfo.FullDescription.LatestNews.News { content = Validate(item.TextContent), href = item.Attributes["href"].Value });
                    }

                var technology = GetTable(doc, "Technology");
                if (technology == null) descr.technology = null;
                else foreach (var item in technology.QuerySelectorAll("tr"))
                    {
                        descr.technology.tecnhology.Add(Validate(item.Children[0].TextContent), Validate(item.Children[1].Children[0].TextContent));
                    }

                var github = GetGitTable(doc);
                if (github == null) descr.github = null;
                else foreach (var item in github.QuerySelectorAll("tr"))
                    {
                        string value = Validate(item.Children[1].TextContent).Replace(" ", "");
                        if (value.Contains("None")) value = "0";
                        descr.github.gitstats.Add(Validate(item.Children[0].TextContent), Int32.Parse(value));
                    }

                //var legalstructure = GetTable(doc, "Legal structure");
                //if (legalstructure == null) descr.legalstructure.structure = null;
                //else foreach (var item in legalstructure.QuerySelectorAll("tr"))
                //    {
                //        descr.legalstructure.structure = Validate(item.Children[1].TextContent);
                //    }


                var latestposts = GetTable(doc, "Latest posts");
                if (latestposts == null) descr.latestposts.posts = null;
                else foreach (var item in latestposts.QuerySelectorAll("a"))
                    {
                        descr.latestposts.posts.Add(new IcoInfo.FullDescription.LatestPosts.Post { content = Validate(item.TextContent), href = Validate(item.Attributes["href"].Value) });
                    }

                var webpresence = GetTable(doc, "Web presence");
                if (webpresence == null || Validate(webpresence.TextContent) == "" || doc.Children[0].TextContent.Contains("Backlinks and popularity data is not available yet")) descr.webpresence = null;
                else
                {
                    var dsraw = webpresence.QuerySelectorAll("tr").Where(q => q.TextContent.Contains("Domain score")).FirstOrDefault().QuerySelector("td").TextContent.Replace(" ", "").Replace("\n", "");
                    descr.webpresence.domainscore = Double.Parse(dsraw, CultureInfo.InvariantCulture);
                    descr.webpresence.backlinks = Int32.Parse(webpresence.QuerySelectorAll("tr").Where(q => q.TextContent.Contains("Backlinks")).FirstOrDefault().QuerySelector("td").TextContent.Replace(" ", ""));
                }
                return descr;
            }
            else return null;
        }

        private IDocument GetDocByUrl(string url)
        {
            var content = GetRequest(url);
            var parser = new HtmlParser();

            return parser.Parse(content);
        }
        private IElement GetTable(IDocument doc, string elemcont)
        {
            try
            {
                var parent = doc.QuerySelectorAll("div").Where(q => q.ClassName == "col-md-6" && q.TextContent.Contains(elemcont) && q.InnerHtml.Contains("h2")).FirstOrDefault();//.FindIndex(q => q.TextContent.Contains("Overview"));
                var testlist = parent.Children.ToList();
                var index = parent.Children.ToList().FindIndex(q => q.TextContent.Contains(elemcont) && q.TagName == "H2");
                var child = parent.Children[index + 1];
                if (child.TagName == "p") return null;
                if (child.TagName.ToLower() != "table") return child = parent.Children[index + 2];
                return child;
            }
            catch (Exception)
            {
                return null;
            }
        }
        private IElement GetGitTable(IDocument doc)
        {
            try
            {
                var gittable = doc.QuerySelectorAll("table").Where(q => q.ClassName == "table asset-list-github" && q.ParentElement.ClassName == "table-responsive").FirstOrDefault();
                return gittable;
            }
            catch (Exception)
            {
                return null;
            } 
        }
        private string GetLink(IDocument doc, string linkname)
        {
            try
            {
                return doc.QuerySelectorAll("a").Where(q => q.TextContent.Contains(linkname) && !q.TextContent.Contains("not available")).FirstOrDefault().Attributes["href"].Value;
            }
            catch (Exception)
            {
                return null;
            }
        }
        private string Validate(string raw)
        {
            if (raw != null)
                return raw.Replace("\n", "").Replace("  ", "").Replace("\t", "");
            else return null;
        }
        private IcoInfo.FullDescription.LatestTweets GetTweetsAndFollowersCount(IcoInfo.FullDescription.LatestTweets tweetclass, string tweeturl)
        {
            try
            {
                var doc = GetDocByUrl(tweeturl);
                var parentposts = doc.QuerySelectorAll("li").Where(q => q.ClassName == "ProfileNav-item ProfileNav-item--tweets is-active").FirstOrDefault();
                tweetclass.posts = Int32.Parse(parentposts.QuerySelectorAll("span").Where(q => q.ClassName == "ProfileNav-value").FirstOrDefault().Attributes["data-count"].Value);
                var parentfollowers = doc.QuerySelectorAll("li").Where(q => q.ClassName == "ProfileNav-item ProfileNav-item--followers").FirstOrDefault();
                tweetclass.followers = Int32.Parse(parentfollowers.QuerySelectorAll("span").Where(q => q.ClassName == "ProfileNav-value").FirstOrDefault().Attributes["data-count"].Value);
                return tweetclass;
            }
            catch (Exception)
            {
                return tweetclass;
            }
        }
        private IcoInfo.FullDescription.YouTube GetYoutube(IcoInfo.FullDescription.YouTube youtube, IDocument doc)
        {
            try
            {
                var ys = doc.QuerySelectorAll("iframe ").Where(q => q.Id == "asset-video-inner").FirstOrDefault() as IHtmlInlineFrameElement;
                youtube.source = ys.Source;
                youtube.channel = doc.QuerySelectorAll("a").Where(q => q.Attributes["href"].Value.Contains("https://www.youtube.com/channel/")).FirstOrDefault().Attributes["href"].Value;
                return youtube;
            }
            catch (Exception)
            {
                //string url;
                //if (ys.Source.Contains("embed")) url = "https://www.youtube.com/watch?v=" + ys.Source.Substring(ys.Source.LastIndexOf('/')+1);

                //var ydoc = GetDocByUrl(ys.Source);
                //youtube.channel = "https://www.youtube.com" + ydoc.QuerySelectorAll("a").Where(q => q.Attributes["href"].Value.Contains("/channel/")).FirstOrDefault().Attributes["href"].Value;

                return youtube;
            }
        }
        private string GetRequest(string url)
        {
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    string responsetext;
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responsetext = reader.ReadToEnd();
                    }
                    return responsetext;
                }
                catch (Exception)
                {
                    Thread.Sleep(2000);
                }
            }
            return null;
        }
    }
}